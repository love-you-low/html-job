### HTML 表单和输入

***<from action= '''>**

First name: <**input** type="text" name="firstname"><**br**>
Last name: <**input** type="text" name="lastname">
</**form**>

## HTML 表单

表单是一个包含表单元素的区域。

表单元素是允许用户在表单中输入内容，比如：文本域（textarea）、下拉列表（select）、单选框（radio-buttons）、复选框（checkbox） 等等。

我们可以使用 **<form>** 标签来创建表单:

### HTML 表单 - 输入元素

多数情况下被用到的表单标签是输入标签 **<input>**。

输入类型是由 **type** 属性定义。

接下来我们介绍几种常用的输入类型。



### 密码字段

密码字段通过标签 **<input type="password">** 来定义



#### 单选按钮（Radio Buttons）

**<input type="radio">** 标签定义了表单的单选框选项:

<**form**>
<**input** type="checkbox" name="vehicle" value="Bike">我喜欢自行车<**br**>
<**input** type="checkbox" name="vehicle" value="Car">我喜欢小汽车
</**form**>

浏览器显示效果如下:

我喜欢自行车
我喜欢小汽车



## 提交按钮(Submit)

**<input type="submit">** 定义了提交按钮。

当用户单击确认按钮时，表单的内容会被传送到服务器。表单的动作属性 **action** 定义了服务端的文件名。

**action** 属性会对接收到的用户输入数据进行相关的处理:

## 实例

<**form** name="input" action="html_form_action.php" method="get">
Username: <**input** type="text" name="user">
<**input** type="submit" value="Submit">
</**form**>



form  输入表单

input  定于输入域

textre 定义文本域

label   标签







#### 